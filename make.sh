GCC=clang
O="-falign-functions=4096 -O2"
$GCC main.c -o main.o -c -g $O &&
$GCC ext.c -o ext.o -fPIC -c -g $O &&
$GCC -shared -o libext.so ext.o &&
$GCC time.c -o time.o -c -g $O &&
$GCC -g $O -o main main.o libext.so time.o &&
LD_LIBRARY_PATH=.:$LD_LIBRARY_PATH ./main
