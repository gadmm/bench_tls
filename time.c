
#ifdef __APPLE__
#include <mach/clock.h>
#include <mach/mach.h>
clock_serv_t cclock;
#else
#include <time.h>
#endif

long long time_counter(void)
{
#if defined(__APPLE__)
  mach_timespec_t t;
  clock_get_time(cclock, &t);
#else
  struct timespec t;
  clock_gettime(CLOCK_MONOTONIC, &t);
#endif
  return (long long)t.tv_sec * (long long)1000000000 + (long long)t.tv_nsec;
}

void init_time_counter(void)
{
#ifdef __APPLE__
  host_get_clock_service(mach_host_self(), SYSTEM_CLOCK, &cclock);
#endif
}
