_Thread_local volatile long long t3 = 0;

void read_extern_thread_local_wrapped(void) { t3; }

#define F(n) _Thread_local volatile long long ext ## n = 0;
#include "two_to_the_ten.h"
#undef F
