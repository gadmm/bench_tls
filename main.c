#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#include <pthread.h>

#include "ext.h"
#include "time.h"

#define BARRIER asm("" : : : "memory")
#define NOINLINE __attribute__((noinline))
#define DUP(x) { x; x; }
#define TWO_TO_THE_TEN(x) DUP(DUP(DUP(DUP(DUP(DUP(DUP(DUP(DUP(DUP(x))))))))))

int num = 1;

void print_duration(long long t)
{
  printf("Duration: %.3fns\n", (double)t / (double)num);
}

pthread_key_t key;
#define F(n) pthread_key_t key ## n;
#include "two_to_the_ten.h"
#undef F

_Thread_local volatile long long t1 = 0;
static _Thread_local volatile long long t2 = 0;
//extern _Thread_local volatile long long t3; // defined in ext.h
/* These attributes have no effect */
__thread volatile long long g1 __attribute__((tls_model("initial-exec"))) = 0;
__thread volatile long long g2 __attribute__((tls_model("local-exec"))) = 0;
__thread volatile long long g3 __attribute__((tls_model("local-dynamic"))) = 0;
__thread volatile long long g4 __attribute__((tls_model("global-dynamic"))) = 0;

void key_init(pthread_key_t *k)
{
  pthread_key_create(k, NULL);
  pthread_setspecific(*k, (void *)1);
}

void init()
{
  init_time_counter();
  /* Calibration check */
  {
    long long start = time_counter();
    usleep(10000);
    long long duration = time_counter() - start;
    print_duration(duration);
    printf("Expected: 10000000.000ns\n");
  }
  /* Hot loop */
  {
    volatile int i = 0;
    volatile int j = 1;
    long long start = time_counter();
    while (time_counter() < start + 1000000000) { j = i + j; i = j - i; }
    print_duration(time_counter() - start);
    printf("Expected: 1000000000.000ns\n");
  }
  /* Count setup */
  num = 0;
  TWO_TO_THE_TEN(num++);
  printf("%d\n", num);
  /* Init TLS */
  key_init(&key);
  t1 = 1;
  t2 = 1;
  t3 = 1;
  /* shuffle twice (otherwise it is accessed later in the same order as it is laid out) */
  pthread_key_t *keys[1024] = {
#define F(n) &(key ## n),
#include "two_to_the_ten.h"
#undef F
  };
#define F(n) key_init(keys[n]);
#include "two_to_the_ten.h"
#undef F
}

#define CACHE_SIZE (32 * 1024 * 1024)

void disturb_cache()
{
  static volatile char *v = NULL;
  if (v == NULL) v = malloc(CACHE_SIZE);
  for (size_t i = 0; i < CACHE_SIZE; i++) {
    v[i] = (char)i;
  }
}

#define read_pthread() pthread_getspecific(key)
#define read_pthread2() pthread_getspecific(key)
#define read_thread_local() t1
#define read_thread_local2() t1
#define read_static_thread_local() t2
#define read_extern_thread_local() t3

#define read_extern_thread_local_wrapped2() read_extern_thread_local_wrapped() // ext.h

#define read_initial_exec() g1
#define read_local_exec() g2
#define read_local_dynamic() g3
#define read_global_dynamic() g4

#define def_bench(F)                                                    \
  NOINLINE void bench_##F() {                                           \
    TWO_TO_THE_TEN(F(); BARRIER;);                                      \
  }

#define NUM_TRIES 5

#define call_bench(F) do {                                              \
    printf("\nbench_" #F "\n");                                         \
    for (int i = 0; i < NUM_TRIES; i++) {                               \
      long long start = time_counter();                                 \
      bench_##F();                                                      \
      long long duration = time_counter() - start;                      \
      print_duration(duration);                                         \
    }                                                                   \
  } while (0)

/* pthread_getspecific */
def_bench(read_pthread);
def_bench(read_pthread2);
/* ELF init-exec model (single mov) */
/* idem */
def_bench(read_thread_local);
def_bench(read_thread_local2);
/* idem */
def_bench(read_static_thread_local);
/* ELF local-exec model, but with compiler optimizations going in the way (faster) */
def_bench(read_extern_thread_local);
/* ELF local-exec model, wrapped in a function call */
def_bench(read_extern_thread_local_wrapped);
def_bench(read_extern_thread_local_wrapped2);
/* following don't work as intended */
/*def_bench(read_initial_exec);
def_bench(read_local_exec);
def_bench(read_local_dynamic);
def_bench(read_global_dynamic);*/

/* ELF local-exec model, without undue compiler optims (check it with gdb) */
NOINLINE void bench_read_ext(void) {
#define F(n) (ext ## n); BARRIER;
#include "two_to_the_ten.h"
#undef F
}

/* Idem; d-cache effects vs. i-cache effects ? */
NOINLINE void bench_read_ext2(void) {
#define F(n) (ext ## n); BARRIER;
#include "two_to_the_ten.h"
#undef F
}

/* Apples to oranges */
NOINLINE void bench_read_pthread_multi(void) {
#define F(n) pthread_getspecific(key ## n);
#include "two_to_the_ten.h"
#undef F
}

/* Idem; d-cache effects vs. i-cache effects ? */
NOINLINE void bench_read_pthread_multi2(void) {
#define F(n) pthread_getspecific(key ## n);
#include "two_to_the_ten.h"
#undef F
}

/* Idem read_ext but with wrapping in a function */
/* Note: unfair comparison to pthread_getspecific (1024 times more
   function bodies): */
#define F(n)                                    \
  NOINLINE void read_ext##n(void) {             \
    (ext ## n);                                 \
  }
#include "two_to_the_ten.h"
#undef F

NOINLINE void bench_read_ext_wrapped(void) {
#define F(n) read_ext##n();
#include "two_to_the_ten.h"
#undef F
}

#define F(n)                                      \
  NOINLINE void read_ext##n##_2(void) {           \
    (ext ## n);                                   \
  }
#include "two_to_the_ten.h"
#undef F

NOINLINE void bench_read_ext_wrapped2(void) {
#define F(n) read_ext##n##_2();
#include "two_to_the_ten.h"
#undef F
}

int main()
{
  init();
  disturb_cache();
  call_bench(read_pthread);
  call_bench(read_pthread2);
  disturb_cache();
  call_bench(read_thread_local);
  call_bench(read_thread_local2);
/*  call_bench(read_static_thread_local);
  call_bench(read_extern_thread_local);*/
  disturb_cache();
  call_bench(read_extern_thread_local_wrapped);
  call_bench(read_extern_thread_local_wrapped2);
/*  call_bench(read_initial_exec);
  call_bench(read_local_exec);
  call_bench(read_local_dynamic);
  call_bench(read_global_dynamic);*/
  disturb_cache();
  call_bench(read_ext);
  call_bench(read_ext2);
  disturb_cache();
  call_bench(read_pthread_multi);
  call_bench(read_pthread_multi2);
/*  disturb_cache();
  call_bench(read_ext_wrapped);
  call_bench(read_ext_wrapped2);*/
/*#ifdef __APPLE__
  mach_port_deallocate(mach_task_self(), cclock);
#endif*/
  return 0;
}
