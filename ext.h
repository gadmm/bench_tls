extern _Thread_local volatile long long t3;

void read_extern_thread_local_wrapped(void);

#define F(n) extern _Thread_local volatile long long ext ## n;
#include "two_to_the_ten.h"
#undef F

